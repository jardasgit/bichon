// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package view

import (
	"testing"
)

func TestTrim(t *testing.T) {
	type TestData struct {
		Input  string
		Output string
		Length int
	}

	data := []TestData{
		{"Daniel Berrange", "Daniel Berrange", 20},
		{"Daniel Berrange", "…Berrange", 9},
		// chomps the whitespace
		{"Daniel Berrange", "…Berrange", 10},
		{"Neal Gompa (ニール・ゴンパ)", "…pa (ニール・ゴンパ)", 20},
		{"Neal Gompa (ニール・ゴンパ)", "…(ニール・ゴンパ)", 17},
		// chomps the whitespace
		{"Neal Gompa (ニール・ゴンパ)", "…(ニール・ゴンパ)", 18},
		{"Neal Gompa (ニール・ゴンパ)", "…・ゴンパ)", 10},
	}

	for _, entry := range data {
		actual := TrimEllipsisFront(entry.Input, entry.Length)
		if actual != entry.Output {
			t.Fatalf("Trim '%s' to %d, expected '%s' but got '%s'",
				entry.Input, entry.Length, entry.Output, actual)
		}
	}
}
