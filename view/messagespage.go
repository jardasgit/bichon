// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell/v2"

	"gitlab.com/bichon-project/tview"
)

type MessagesPageListener interface {
	MessagesPageQuit()
}

type MessagesPage struct {
	*tview.Frame
	ActionMap

	Application *tview.Application
	Listener    MessagesPageListener

	Content *tview.TextView
}

func NewMessagesPage(app *tview.Application, listener MessagesPageListener) *MessagesPage {
	content := tview.NewTextView()
	layout := tview.NewFrame(content).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &MessagesPage{
		Frame:     layout,
		ActionMap: NewActionHandler("messages-page", nil),

		Application: app,
		Listener:    listener,
		Content:     content,
	}

	page.registerActions()

	return page
}

func (page *MessagesPage) GetName() string {
	return "messages"
}

func (page *MessagesPage) GetKeyShortcuts() string {
	return page.ActionMap.FormatSummary(
		"quit",
		"clear")
}

func (page *MessagesPage) Info(msg string) {
	page.Content.Write([]byte(msg))
	page.Content.Write([]byte("\n"))
}

func (page *MessagesPage) Activate() {
	page.Application.SetFocus(page.Content)
}

func (page *MessagesPage) registerActions() {
	page.ActionMap.RegisterAction(
		"quit", "Index",
		func() bool {
			page.Listener.MessagesPageQuit()
			return true
		},
		NewActionRune('q', tcell.ModNone),
		NewActionKey(tcell.KeyEscape, tcell.ModNone),
	)
	page.ActionMap.RegisterAction(
		"clear", "Clear messages",
		func() bool {
			page.Content.Clear()
			return true
		},
		NewActionRune('c', tcell.ModNone),
	)
}
