// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"time"

	"gitlab.com/bichon-project/tview"
)

type MessageBar struct {
	App    *tview.Application
	Text   *tview.TextView
	Expiry *time.Time
	Clear  chan time.Duration
}

func NewMessageBar(app *tview.Application) *MessageBar {
	msgbar := &MessageBar{
		App:   app,
		Text:  tview.NewTextView().SetDynamicColors(true),
		Clear: make(chan time.Duration, 1),
	}

	go msgbar.clearTimer()

	return msgbar
}

func (msgbar *MessageBar) clearTimer() {
	t := time.NewTicker(time.Second)
	for {
		if t != nil {
			select {
			case secs := <-msgbar.Clear:
				t.Stop()
				t = time.NewTicker(secs)
			case _ = <-t.C:
				msgbar.App.QueueUpdateDraw(func() {
					msgbar.Info("")
				})
				t.Stop()
				t = nil
			}
		} else {
			select {
			case secs := <-msgbar.Clear:
				t = time.NewTicker(secs)
			}
		}
	}
}

func (msgbar *MessageBar) Info(msg string) {
	color := GetStyleMarker(
		ELEMENT_MESSAGES_TEXT,
		ELEMENT_MESSAGES_FILL,
		ELEMENT_MESSAGES_ATTR)
	if msg == "" {
		if msgbar.Expiry != nil &&
			msgbar.Expiry.Before(time.Now()) {
			msgbar.Text.SetText(color + "")
			msgbar.Expiry = nil
		}
	} else {
		dur := time.Second * 3
		then := time.Now().Add(dur)
		msgbar.Expiry = &then
		msgbar.Clear <- dur
		msgbar.Text.SetText(color + tview.Escape(msg))
	}
}
