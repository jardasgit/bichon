// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package xdg

import (
	"github.com/casimir/xdg-go"
)

var xdgapp xdg.App

func init() {
	xdgapp = xdg.App{
		Name: "bichon",
	}
}

func ConfigPath(name string) string {
	return xdgapp.ConfigPath(name)
}

func CachePath(name string) string {
	return xdgapp.CachePath(name)
}
