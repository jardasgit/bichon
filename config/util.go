// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019-2020 Red Hat, Inc.

package config

import (
	"os"

	"github.com/go-ini/ini"
)

func AtomicSave(cfg *ini.File, filename string) error {
	err := cfg.SaveTo(filename + ".new")
	if err != nil {
		return err
	}

	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}

	_, err = cfg.WriteTo(file)
	if err != nil {
		file.Close()
		os.Remove(filename + ".new")
		return err
	}

	err = file.Sync()
	if err != nil {
		file.Close()
		os.Remove(filename + ".new")
		return err
	}

	err = file.Close()
	if err != nil {
		os.Remove(filename + ".new")
		return err
	}

	err = os.Rename(filename+".new", filename)
	if err != nil {
		os.Remove(filename + ".new")
		return err
	}
	return nil

}
